package com.xuanner.seq;

import com.alibaba.druid.pool.DruidDataSource;
import com.xuanner.seq.range.impl.db.DbSeqRangeMgr;
import com.xuanner.seq.sequence.impl.DefaultRangeSequence;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by xuan on 2018/1/10.
 */
public class DbTest_Api {

    private com.xuanner.seq.sequence.Sequence userSeq;

    @Before
    public void setup() {
        //数据源
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl("xxx");
        dataSource.setUsername("xxx");
        dataSource.setPassword("xxx");
        dataSource.setMaxActive(300);
        dataSource.setMinIdle(50);
        dataSource.setInitialSize(2);
        dataSource.setMaxWait(500);
        //利用DB获取区间管理器
        DbSeqRangeMgr dbSeqRangeMgr = new DbSeqRangeMgr();
        dbSeqRangeMgr.setDataSource(dataSource);//数据源[必选]
        dbSeqRangeMgr.setTableName("sequence");//表名[可选] 默认：sequence
        dbSeqRangeMgr.setRetryTimes(100);//更新失败重试次数[可选] 默认：100
        dbSeqRangeMgr.setStep(100);//每次取数步长[可选] 默认：1000
        dbSeqRangeMgr.init();
        //构建序列号生成器
        DefaultRangeSequence defaultRangeSequence = new DefaultRangeSequence();
        defaultRangeSequence.setName("user");
        defaultRangeSequence.setSeqRangeMgr(dbSeqRangeMgr);
        userSeq = defaultRangeSequence;
    }

    @Test
    public void test() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            System.out.println("++++++++++id:" + userSeq.nextValue());
        }
        System.out.println("interval time:" + (System.currentTimeMillis() - start));
    }
}
