package com.xuanner.seq;

import com.xuanner.seq.sequence.Sequence;
import com.xuanner.seq.sequence.impl.SnowflakeSequence;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by xuan on 2018/5/9.
 */
public class SnowflakeTest_Api {

    private Sequence userSeq;

    @Before
    public void setup() {

        SnowflakeSequence snowflakeSequence = new SnowflakeSequence();
        //用这两个值来确定机器的唯一标志，每个字段值返回在[0,31]之间
        snowflakeSequence.setDatacenterId(1);
        snowflakeSequence.setWorkerId(2);
        userSeq = snowflakeSequence;
    }

    @Test
    public void test() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            userSeq.nextValue();
            //System.out.println("++++++++++id:" + userSeq.nextValue());
        }
        System.out.println("++++++++++lastId:" + userSeq.nextValue());
        long t = System.currentTimeMillis() - start;
        System.out.println("interval time:" + t);
        System.out.println("qps:" + 10000000 * 1000 / t);
    }
}
